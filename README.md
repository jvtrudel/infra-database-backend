# Codex Medical for Ukrania


Traduction de terme médicaux, médicaments et instruments. Entre français, ukrainien, anglais, russe, espagnol...

![architecture](./codexmedua.svg)



## Sources d' information


  - [Notes et informations par François Pelletier](https://github.com/franc00018/ukrainian_medicine_names_fip_csv)
  - [Liste équivalence (googl sheet) de Valentin Kravtchenko](https://docs.google.com/spreadsheets/d/1m3LampIajhMrl3ZrUytifBrHyt1YhOvYfSmnX0zH_Gk/edit#gid=0)
  
  
## random info

  - https://htaglossary.net/
  - https://www.vidal.ru/
  - https://fr.wikipedia.org/wiki/Classification_ATC
  - https://www.snomed.org/
  - https://docs.google.com/spreadsheets/d/1m3LampIajhMrl3ZrUytifBrHyt1YhOvYfSmnX0zH_Gk/edit#gid=0
  - https://ebauche.facil.services/code/#/2/code/edit/WlSG2kOR-rqTNFeH4hbDdLGg/
  - https://www.rsc.org/merck-index
  - https://www.chemsrc.com/
  - https://go.drugbank.com/
